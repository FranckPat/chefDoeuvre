<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180606122634 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE films (id INT AUTO_INCREMENT NOT NULL, acteurs_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, url_image LONGTEXT NOT NULL, realisation VARCHAR(255) NOT NULL, pays_doriginie VARCHAR(255) NOT NULL, vues INT NOT NULL, synopsis LONGTEXT NOT NULL, rating VARCHAR(9) NOT NULL, date_de_sortie INT NOT NULL, during INT NOT NULL, quality VARCHAR(6) NOT NULL, bande_annonce LONGTEXT NOT NULL, url_film LONGTEXT NOT NULL, INDEX IDX_CEECCA5171A27AFC (acteurs_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, categories_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acteurs (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE films ADD CONSTRAINT FK_CEECCA5171A27AFC FOREIGN KEY (acteurs_id) REFERENCES acteurs (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE films DROP FOREIGN KEY FK_CEECCA5171A27AFC');
        $this->addSql('DROP TABLE films');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE acteurs');
    }
}
