<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Films;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class AccueilController extends Controller
{
    /**
     * @Route("/", name="accueil")
     */
    public function categories()
    {
        $films = $this->getDoctrine()->getRepository(Films::class)->findAll();
        $categories = $this->getDoctrine()->getRepository(Categories::class)->findAll();
        return $this->render('index.html.twig', [
            'allCategories'=>$categories,
            'allFilms'=>$films
        ]);
    }
}
