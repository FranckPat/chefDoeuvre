<?php

namespace App\Controller;

use App\Entity\Acteurs;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/acteurs")
 */
class ActeursController extends Controller
{
    /**
     * @Route("/")
     */
    public function getAllActeurs()
    {
        $acteurs = $this->getDoctrine()->getRepository(Acteurs::class)->findAll();
        return $this->render('acteurs/index.html.twig', [
            'controller_name' => 'ActeursController',
            'acteurs'=>$acteurs
        ]);
    }
    /**
     * @Route("/update/{id}")
     */
    public function update(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $actors = $entityManager->getRepository(Acteurs::class)->find($id);
        $form = $this->createFormBuilder($actors)
            ->add(
                'FirstName',
                TextType::class
            )
            ->add(
                'Surname',
                TextType::class
            )
            ->add('save',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //ENVOI UPDATE BDD
            $entityManager->flush();
            return $this->redirect('/acteurs');
        }
        return $this->render('acteurs/update.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @Route("/createOne")
     */
    public function create(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $actors = new Acteurs();
        $form = $this->createFormBuilder($actors)
            ->add(
                'FirstName',
                TextType::class
            )
            ->add(
                'Surname',
                TextType::class
            )
            ->add('save',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        $task = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {
            //ENVOI UPDATE BDD
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirect('/acteurs');
        }
        return $this->render('acteurs/create.html.twig',['form'=>$form->createView()]);
    }
    /**
     * @Route("/delete/{id}")
     */
    public function delete(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $actors = $entityManager->getRepository(Acteurs::class)->find($id);
        $entityManager->remove($actors);
        $entityManager->flush();
        return $this->redirect('/acteurs');
    }
}
