<?php

namespace App\Controller;

use App\Entity\Categories;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categories")
 */

class CategoriesController extends Controller
{
    /**
     * @Route("/create", name="categories")
     */
    public function index(Request $request)
    {
        $categories = new Categories();
        $form = $this->createFormBuilder($categories)
            ->add(
                'CategoriesName',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Nom de categorie'
                        )
                )
            )
            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
            if (strlen($task->getCategoriesName()) > 5) {
                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($task);
                $entityManager->flush();
                return $this->redirect('/');
            } else {
                $erreur = 'Nouvelle erreur';
                return $this->render('erreur.html.twig', ['erreur' => $erreur]);
            }
        }
        return $this->render('categories/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/toute")
     */
    public function allCategories()
    {
        $categories = $this->getDoctrine()->getRepository(Categories::class)->findAll();
        return $this->render('categories/read.html.twig',['allCategories'=>$categories]);
    }

    /**
     * @Route("/update/{id}")
     */
    public function update(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $contacts = $this->getDoctrine()->getRepository(Categories::class)->find($id);
        $form = $this->createFormBuilder($contacts)
            ->add('CategoriesName', TextType::class)
            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //ENVOI UPDATE BDD
            $entityManager->flush();
            return $this->redirect('/');
        }
        return $this->render('categories/update.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @Route("/delete/{id}")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categories = $entityManager->getRepository(Categories::class)->find($id);
        $entityManager->remove($categories);
        $entityManager->flush();
        return $this->redirect('/');
    }
    /**
     * @Route("/find/{id}")
     */
    public function findByOne($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $category = $entityManager->getRepository(Categories::class)->find($id);
        $film = $entityManager->getRepository(Categories::class)->findByCategory($id);
        $categories = $entityManager->getRepository(Categories::class)->findAll();
        return $this->render('categories/readByOne.html.twig',[
            'categorie'=>$category,
            'categories'=>$categories,
            'films'=>$film
        ]);
    }
}