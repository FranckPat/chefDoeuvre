<?php

namespace App\Controller;

use App\Entity\Acteurs;
use App\Entity\Films;
use App\Entity\Categories;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * @Route("/films")
 */
class FilmsController extends Controller
{
    /**
     * @Route("/select/{id}", name="films")
     */
    public function index($id)
    {
        $film = $this->getDoctrine()->getRepository(Films::class)->find($id);
        $allCategory = $this->getDoctrine()->getRepository(Categories::class)->findAll();
        return $this->render('films/index.html.twig', [
            'film' =>$film,
            'categories'=>$allCategory
        ]);
    }

    /**
     * @Route("/createOne")
     */
    public function create(Request $request){
        $NewFilm = new Films();
        $categories = $this->getDoctrine()->getRepository(Categories::class)->findAll();
        $form = $this->createFormBuilder($NewFilm)
            ->add(
                'Titre',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Nom du Film'
                        )
                )
            )
            ->add(
                'urlImage',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Image d\'affichage du film'
                        )
                )
            )


            ->add(
                'Realisation',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Nom et Prénom du Réalisateur'
                        )
                )
            )

            ->add(
                'PaysDOriginie',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Pays de réalisation'
                        )
                )
            )
            ->add(
                'Vues',
                IntegerType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Nombre de vues de votre films'
                        )
                )
            )

            ->add(
                'Synopsis',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Nombre de vues de votre films'
                        )
                )
            )

            ->add(
                'Rating',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'9',
                            'placeholder'=>'Nombre de vues de votre films'
                        )
                )
            )
            ->add(
                'DateDeSortie',
                IntegerType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'4',
                            'placeholder'=>'Année de sortie de votre film'
                        )
                )
            )
            ->add(
                'During',
                IntegerType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'11',
                            'placeholder'=>'Durée du film'
                        )
                )
            )

            ->add(
                'BandeAnnonce',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Pas de feature SVP',
                        )
                )
            )
            ->add(
                'UrlFilm',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Mettez votre film ici',
                        )
                )
            )
            ->add(
                'quality',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'9',
                            'placeholder'=>'HD ,HDRIP, HDTV, TS, DvDrip, BDRip',
                        )
                )
            )


            ->add(
                'Categories',
                EntityType::class, array(
                'label' => 'catégorie',
                'class' => Categories::class,
                'choice_label' => 'categoriesName'))

            ->add(
                'Acteurs',
                EntityType::class, array(
                'label' => 'acteurs',
                'class' => Acteurs::class,
                'choice_label' => 'FirstName'))

            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
            if (strlen($task->getDateDeSortie()) > 3) {
                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($task);
                $entityManager->flush();
                return $this->redirect('/');
            } else {
                $erreur = 'Nouvelle erreur';
                return $this->render('erreur/erreur.html.twig', ['erreur' => $erreur]);
            }
        }
        return $this->render('films/create.html.twig',['form'=>$form->createView()]);
    }
    /**
     * @Route("/update/{id}")
     */
    public function update(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $films = $this->getDoctrine()->getRepository(Films::class)->find($id);
        $form = $this->createFormBuilder($films)
            ->add(
                'Titre',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Nom du Film'
                        )
                )
            )
            ->add(
                'urlImage',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Image d\'affichage du film'
                        )
                )
            )
            ->add(
                'Realisation',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Nom et Prénom du Réalisateur'
                        )
                )
            )
            ->add(
                'PaysDOriginie',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Pays de réalisation'
                        )
                )
            )
            ->add(
                'Vues',
                IntegerType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'255',
                            'placeholder'=>'Nombre de vues de votre films'
                        )
                )
            )
            ->add(
                'Synopsis',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Nombre de vues de votre films'
                        )
                )
            )
            ->add(
                'Rating',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'9',
                            'placeholder'=>'Nombre de vues de votre films'
                        )
                )
            )
            ->add(
                'DateDeSortie',
                IntegerType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'4',
                            'placeholder'=>'Année de sortie de votre film'
                        )
                )
            )
            ->add(
                'During',
                IntegerType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'11',
                            'placeholder'=>'Durée du film'
                        )
                )
            )
            ->add(
                'BandeAnnonce',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Pas de feature SVP',
                        )
                )
            )
            ->add(
                'UrlFilm',
                TextareaType::class,
                array(
                    'attr'=>
                        array(
                            'placeholder'=>'Mettez votre film ici',
                        )
                )
            )
            ->add(
                'quality',
                TextType::class,
                array(
                    'attr'=>
                        array(
                            'max-length'=>'9',
                            'placeholder'=>'HD ,HDRIP, HDTV, TS, DvDrip, BDRip',
                        )
                )
            )
            ->add(
                'Categories',
                EntityType::class, array(
                'label' => 'catégorie',
                'class' => Categories::class,
                'choice_label' => 'categoriesName'))

            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //ENVOI UPDATE BDD
            $entityManager->flush();
            return $this->redirect('/');
        }
        return $this->render('films/update.html.twig',['form'=>$form->createView()]);
    }
    /**
     * @Route("/delete/{id}")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Films::class)->find($id);
        $entityManager->remove($product);
        $entityManager->flush();
        return $this->redirect('/');

    }
}
