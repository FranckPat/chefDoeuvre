<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActeursRepository")
 */
class Acteurs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Films", mappedBy="acteurs")
     */
    private $film_realise;

    public function __construct()
    {
        $this->film_realise = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return Collection|Films[]
     */
    public function getFilmRealise(): Collection
    {
        return $this->film_realise;
    }

    public function addFilmRealise(Films $filmRealise): self
    {
        if (!$this->film_realise->contains($filmRealise)) {
            $this->film_realise[] = $filmRealise;
            $filmRealise->setActeurs($this);
        }

        return $this;
    }

    public function removeFilmRealise(Films $filmRealise): self
    {
        if ($this->film_realise->contains($filmRealise)) {
            $this->film_realise->removeElement($filmRealise);
            // set the owning side to null (unless already changed)
            if ($filmRealise->getActeurs() === $this) {
                $filmRealise->setActeurs(null);
            }
        }

        return $this;
    }
}
