<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesRepository")
 */
class Categories
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $categories_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Films", mappedBy="categories")
     */
    private $categories_film;

    public function __construct()
    {
        $this->categories_film = new ArrayCollection();
        $this->film = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategoriesName(): ?string
    {
        return $this->categories_name;
    }

    public function setCategoriesName(string $categories_name): self
    {
        $this->categories_name = $categories_name;

        return $this;
    }

    /**
     * @return Collection|Films[]
     */
    public function getCategoriesFilm(): Collection
    {
        return $this->categories_film;
    }

    public function addCategoriesFilm(Films $categoriesFilm): self
    {
        if (!$this->categories_film->contains($categoriesFilm)) {
            $this->categories_film[] = $categoriesFilm;
            $categoriesFilm->setCategories($this);
        }

        return $this;
    }

    public function removeCategoriesFilm(Films $categoriesFilm): self
    {
        if ($this->categories_film->contains($categoriesFilm)) {
            $this->categories_film->removeElement($categoriesFilm);
            // set the owning side to null (unless already changed)
            if ($categoriesFilm->getCategories() === $this) {
                $categoriesFilm->setCategories(null);
            }
        }

        return $this;
    }
}
