<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilmsRepository")
 */
class Films
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $url_image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $realisation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pays_DOriginie;

    /**
     * @ORM\Column(type="integer")
     */
    private $vues;

    /**
     * @ORM\Column(type="text")
     */
    private $synopsis;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $rating;

    /**
     * @ORM\Column(type="integer")
     */
    private $dateDeSortie;

    /**
     * @ORM\Column(type="integer")
     */
    private $during;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $quality;

    /**
     * @ORM\Column(type="text")
     */
    private $bande_annonce;

    /**
     * @ORM\Column(type="text")
     */
    private $url_Film;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Acteurs", inversedBy="film_realise")
     */
    private $acteurs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="categories_film")
     */
    private $categories;

    public function getId()
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getUrlImage(): ?string
    {
        return $this->url_image;
    }

    public function setUrlImage(string $url_image): self
    {
        $this->url_image = $url_image;

        return $this;
    }

    public function getRealisation(): ?string
    {
        return $this->realisation;
    }

    public function setRealisation(string $realisation): self
    {
        $this->realisation = $realisation;

        return $this;
    }

    public function getPaysDOriginie(): ?string
    {
        return $this->pays_DOriginie;
    }

    public function setPaysDOriginie(string $pays_DOriginie): self
    {
        $this->pays_DOriginie = $pays_DOriginie;

        return $this;
    }

    public function getVues(): ?int
    {
        return $this->vues;
    }

    public function setVues(int $vues): self
    {
        $this->vues = $vues;

        return $this;
    }

    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    public function setSynopsis(string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getDateDeSortie(): ?int
    {
        return $this->dateDeSortie;
    }

    public function setDateDeSortie(int $dateDeSortie): self
    {
        $this->dateDeSortie = $dateDeSortie;

        return $this;
    }

    public function getDuring(): ?int
    {
        return $this->during;
    }

    public function setDuring(int $during): self
    {
        $this->during = $during;

        return $this;
    }

    public function getQuality(): ?string
    {
        return $this->quality;
    }

    public function setQuality(string $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function getBandeAnnonce(): ?string
    {
        return $this->bande_annonce;
    }

    public function setBandeAnnonce(string $bande_annonce): self
    {
        $this->bande_annonce = $bande_annonce;

        return $this;
    }

    public function getUrlFilm(): ?string
    {
        return $this->url_Film;
    }

    public function setUrlFilm(string $url_Film): self
    {
        $this->url_Film = $url_Film;

        return $this;
    }

    public function getActeurs(): ?Acteurs
    {
        return $this->acteurs;
    }

    public function setActeurs(?Acteurs $acteurs): self
    {
        $this->acteurs = $acteurs;

        return $this;
    }

    public function getCategories(): ?Categories
    {
        return $this->categories;
    }

    public function setCategories(?Categories $categories): self
    {
        $this->categories = $categories;

        return $this;
    }
}
